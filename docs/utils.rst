Utils
=====

Utilities for Earth detectors (in functions_earth.py).

.. automodule:: gwtoolbox.functions_earth

.. autosummary::

    :toctree: functions_earth

    ind_masses
    sym_ratio
    chirp_mass
    rho_sq_core
    fis_inf_matr
    errors
    FIM_conv_matr


Class to describe the cosmological model (in cosmology.py).

.. automodule:: gwtoolbox.cosmology
     
   .. autoclass:: gwtoolbox.cosmology.Cosmology
     :members:


Plotting functions (in plots.py and plotshtml.py).


.. automodule:: gwtoolbox.plots

.. autosummary::

    :toctree: plots

    plot_vs
    plot_hist
    plot_noise
    plot_noise_withdefault
