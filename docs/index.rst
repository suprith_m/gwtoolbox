.. gwtoolbox documentation master file, created by
   sphinx-quickstart on Sun Aug 25 22:47:18 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

gwtoolbox
=========

Introduction
------------

**gwtoolbox** is a custom Python module to simulate gravitational wave Universe.
We implemented **gwtoolbox** in web application `gw-universe.org <https://gw-universe.org>`_


.. toctree::
   :maxdepth: 2

   install
   examples

API   
---
.. toctree::
   :maxdepth: 2
    
   tools
   detectors
   sources
   utils


