#!/bin/sh
echo "installing MLDC common packages"
python3 gwtoolbox/MLDC-master/Packages/common/setup.py install
echo "installing MLDC Waveforms: fast GB"
python3 gwtoolbox/MLDC-master/Waveforms/fastGB/setup.py install 
echo "installing MLDC Waveforms: IMR_PhenomD"
python3 gwtoolbox/MLDC-master/Waveforms/MBH_IMR/IMRPhenomD/setup.py install
echo "installing AAKwrapper from EMRI_Klugdge_Suite"
echo "installing the GWToolbox"
python3 setup.py install  
